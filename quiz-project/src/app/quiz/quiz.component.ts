import { Component, OnInit } from '@angular/core';
import { QuizService } from '../quiz.service';
import { Question } from '../question';
import { Option } from '../option';

@Component({
  selector: 'app-quiz',
  templateUrl: './quiz.component.html',
  styleUrls: ['./quiz.component.css'],
  providers: [QuizService]
})


export class QuizComponent implements OnInit {

  mode = 'quiz';
  questions: [Question];

  constructor(private quizService: QuizService) { }

  ngOnInit() {
    this.quizService.getAllQuestions().subscribe(response => {
      console.log(response);
      this.questions = response
    }); 
    this.mode = 'quiz';
  }

  onSelect(question: Question, option: Option) {
    question.options.forEach(eachOption => {
      if (eachOption.id != option.id) {
        eachOption.selected = false;
      } else {
        eachOption.selected = true
      }
    })
  }

  isCorrectAnswer(question: Question) {
    if (question.options.every(eachOption => (eachOption.selected === eachOption.isCorrect))) {
      return 'correct';
    } else {
      return 'wrong';
    }
  }

  onSubmit() {
    this.mode = 'result';
  }

}
