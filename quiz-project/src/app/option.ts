export class Option {
    id: number;
    answer: string;
    isCorrect: boolean;
    selected: boolean;

    constructor(data: any) {
        this.id = data.id;
        this.answer = data.answer;
        this.isCorrect = data.isCorrect;
    }

}
