import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Question } from './question';

@Injectable({
  providedIn: 'root'
})
export class QuizService {

  url = 'http://localhost:9000/questions';

  constructor(private http: HttpClient) { }

  getAllQuestions() {
    return this.http.get<[Question]>(this.url);
  }


}
