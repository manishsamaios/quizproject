var express = require('express');
var bodyParser = require('body-parser');
var cors = require('cors');
var MongoClient = require('mongodb').MongoClient;

var app = express();
app.use(cors());
app.use(bodyParser());

app.get('/questions/',function(req,resp){        		//Display all rows from mongod
    MongoClient.connect("mongodb://localhost:27017/QuizDB", function(err, db) {
        if(!err) {
            db.collection('questions').find().toArray(function(err, docs){
                console.log(docs);
              resp.send(JSON.stringify(docs));
      });  }
      });
});

app.listen(9000, () => console.log('API started listening...'));